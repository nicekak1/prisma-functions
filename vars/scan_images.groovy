def call() {
    prismaCloudScanImage ca: '',
    cert: '',
    dockerAddress: 'unix:///var/run/docker.sock',
    image: "${imagescan}",
    key: '',
    logLevel: 'info',
    podmanPath: '',
    project: '',
    resultsFile: 'prisma-cloud-scan-results.json',
    ignoreImageBuildTime:true
}