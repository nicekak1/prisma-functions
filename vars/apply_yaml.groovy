def call(user,pass,clustername) {
    sh """
        oc login -u "${user}" -p "${pass}" --insecure-skip-tls-verify https://api.issue.dso.local:6443 
        oc project twistlock-nforce
        oc project
        oc apply -f daemonset-"${clustername}".yaml
        oc get po
        """
}