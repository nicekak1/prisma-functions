def call(pass) {
    sh """
                    echo "***Logging In***"
                    docker login -u pittimonr -p "${pass}"
                    echo "***Pushing Image***"
                    docker push pittimonr/pythom-test:$BUILD_TAG
                    docker rmi pittimonr/pythom-test:$BUILD_TAG -f
                """
}